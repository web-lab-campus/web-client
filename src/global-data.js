export default {
  state: {
    webConfig: {},
    user: null,
    alert: []
  },

  // Set global web config
  setWebConfig (config) {
    this.state.webConfig = config
  },

  // Set logged user
  setUser (user) {
    this.state.user = user
  },

  // Remove logged user
  clearUser () {
    this.state.user = null
  },

  // Add an alert
  addAlert (type, name, description) {
    const hash = `${Date.now()}-${Math.floor(Math.random() * 1000)}`
    const timeout = 100 * (name.length + description.length) + 1000

    this.state.alert.push({ type, name, description, hash })

    setTimeout(() => {
      const index = this.state.alert.findIndex(l => l.hash === hash)
      if (typeof index !== 'undefined') this.removeAlert(index)
    }, timeout)
  },

  // remove an alert
  removeAlert (index) {
    this.state.alert.splice(index, 1)
  }
}
