import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: () => ({ x: 0, y: 0 }),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/access-control',
      name: 'access-control',
      component: () => import(/* webpackChunkName: "access-control" */ './views/AccessControl.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "auth" */ './views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "auth" */ './views/Register.vue')
    },
    {
      path: '/user/:id/settings',
      name: 'user.settings',
      component: () => import(/* webpackChunkName: "user" */ './views/user/Settings.vue')
    },
    {
      path: '/user/:id',
      name: 'user.show',
      component: () => import(/* webpackChunkName: "user" */ './views/user/Show.vue')
    },
    {
      path: '/information',
      name: 'info.index',
      component: () => import(/* webpackChunkName: "info" */ './views/info/Index.vue')
    },
    {
      path: '/information/create',
      name: 'info.create',
      component: () => import(/* webpackChunkName: "info" */ './views/info/Editor.vue')
    },
    {
      path: '/information/:id',
      name: 'info.show',
      component: () => import(/* webpackChunkName: "info" */ './views/info/Show.vue')
    },
    {
      path: '/information/:id/edit',
      name: 'info.edit',
      component: () => import(/* webpackChunkName: "info" */ './views/info/Editor.vue')
    }
  ]
})
