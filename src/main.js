import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './global-data'
import './registerServiceWorker'
import './main.scss'

Vue.config.productionTip = false

new Vue({
  router,
  data: store.state,
  render: h => h(App)
}).$mount('#app')
